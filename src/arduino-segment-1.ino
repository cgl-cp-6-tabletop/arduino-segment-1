#include <arduino.h>

// Use macros for constants to save memory space.

// If this is defined, allows debug logging through DEBUG_LOG().
#define AR_DEBUG

#if defined( AR_DEBUG )
	#define DEBUG_LOG(statement) Serial.println(statement)
#else
	#define DEBUG_LOG(statement)
#endif


// How many times per second the Arduino should send data to the PC.
#define POLL_RATE 5
// The line ending used by the game engine.
#define LINE_ENDING "\r\n"
// The prefix that denotes a command.
#define OZOMATLI_COMMAND_PREFIX "oz/"

#define REGISTER_GAME_COMMAND_NAME "reg"

bool isRegistered = true;


// Stores all received input. Used to look for line endings.
String inputBuffer;


// Input pins
#define LASER_INPUT_D 2
#define SHOCK_INPUT_D 3
#define VIBRATION_INPUT_D 4
#define DISTANCE_INPUT_D 5

#define ROTATOR_INPUT_A 0
#define AMBIENT_LIGHT_INPUT_A 1
#define LIGHT_INPUT_A 2
#define SLIDER_INPUT_1_A 3
#define SLIDER_INPUT_2_A 4
#define WATER_INPUT_A 15

// Forward declarations.
void onReceiveLine( String line );
void onReceiveCommand( String commandName, String commandContent );
void readLines();
void registerPins();


void setup()
{
	registerPins();
	Serial.begin( 9600 );
	Serial.flush();
}

void onReceiveLine( String line )
{
	if( line.indexOf( OZOMATLI_COMMAND_PREFIX ) == 0 )
	{
		String wholeCommand = line.substring( 3 );
		int commandContentOpenIndex = wholeCommand.indexOf( '{' );
		int commandContentCloseIndex = wholeCommand.lastIndexOf( "};" );
		
		String commandName = wholeCommand.substring( 0, commandContentOpenIndex );
		String commandContent = wholeCommand.substring( commandContentOpenIndex + 1, commandContentCloseIndex );

		onReceiveCommand( commandName, commandContent );
	}
}

void onReceiveCommand( String commandName, String commandContent )
{
	if( commandName == REGISTER_GAME_COMMAND_NAME )
	{
		isRegistered = true;
	}
}

void readLines()
{
	if( Serial.available() <= 0 )
	{
		return;
	}

	String currentInput = Serial.readString();
	bool couldAppend = inputBuffer.concat( currentInput );
	if( couldAppend == false )
	{
		DEBUG_LOG( "ERROR: Could not append buffers!" );
		return;
	}

	while(true)
	{
		int lineEndingIndex = inputBuffer.indexOf( LINE_ENDING );
		if( lineEndingIndex <= 0 )
		{
			break;
		}

		String line = inputBuffer.substring( 0, lineEndingIndex );
		onReceiveLine( line );
		inputBuffer.remove( 0, lineEndingIndex + sizeof( LINE_ENDING ) - 1 );
	}
}

void registerPins()
{
	pinMode( LED_BUILTIN, OUTPUT );

	// Register only digital pins.
	pinMode( LASER_INPUT_D, INPUT );
	pinMode( SHOCK_INPUT_D, INPUT );
	pinMode( VIBRATION_INPUT_D, INPUT );
	pinMode( DISTANCE_INPUT_D, INPUT );
}

void loop()
{
	readLines();

	if( isRegistered )
	{
		String sensorOutputSerial = "ozModule{0";

		// Spider webs.
		bool isLaserInterrupted = digitalRead( LASER_INPUT_D );
		sensorOutputSerial += "," + String( isLaserInterrupted );

		// Slider 1.
		int slider_1 = analogRead( SLIDER_INPUT_1_A );
		sensorOutputSerial += "," + String( slider_1 );

		// Slider 2.
		int slider_2 = analogRead( SLIDER_INPUT_2_A );
		sensorOutputSerial += "," + String( slider_2 );

		// Rotating tree.
		int rotation = analogRead( ROTATOR_INPUT_A );
		sensorOutputSerial += "," + String( rotation );

		// Light sensor in pyramid.
		int lightValue = analogRead( AMBIENT_LIGHT_INPUT_A );
		sensorOutputSerial += "," + String( lightValue );

		sensorOutputSerial += "};";
		DEBUG_LOG( sensorOutputSerial );
	}
}